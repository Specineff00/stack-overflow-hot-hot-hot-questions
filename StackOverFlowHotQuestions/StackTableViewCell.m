//
//  StackTableViewCell.m
//  StackOverFlowHotQuestions
//
//  Created by Yogesh N Ramsorrrun on 19/05/2016.
//  Copyright © 2016 Yogesh N Ramsorrrun. All rights reserved.
//

#import "StackTableViewCell.h"

@implementation StackTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIFont *myFont = [ UIFont fontWithName: @"Lato-Light" size: 20.0 ];
    self.titleLabel.font  = myFont;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
