//
//  StackTableViewCell.h
//  StackOverFlowHotQuestions
//
//  Created by Yogesh N Ramsorrrun on 19/05/2016.
//  Copyright © 2016 Yogesh N Ramsorrrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *voteCount;
@property (weak, nonatomic) IBOutlet UILabel *tags;

@end
