//
//  ViewController.m
//  StackOverFlowHotQuestions
//
//  Created by Yogesh N Ramsorrrun on 18/05/2016.
//  Copyright © 2016 Yogesh N Ramsorrrun. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <NSString+HTML.h>
#import "StackTableViewCell.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSDictionary *questionDictionary;
@property (strong, nonatomic) NSString *string;
@property (strong, nonatomic) NSArray *JsonArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UITableView *stackTableView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshControllerSetup];
    [self getTheInfo];
}

- (void)getTheInfo {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"https://api.stackexchange.com/2.2/questions?fromdate=1463011200&todate=1463616000&order=desc&sort=activity&site=stackoverflow"
      parameters:nil
         success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
//             NSLog(@"JSON: %@",responseObject);
             
             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                 NSDictionary *JsonDictionary = responseObject;
                  _JsonArray = [JsonDictionary valueForKey:@"items"];
                 [_stackTableView reloadData];
             }
             
         } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
             NSLog(@"Error");
         }];
}

#pragma mark TableView Setup

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_JsonArray count];
}

- (void)refresh {
    [_stackTableView reloadData];
    [_refreshControl endRefreshing];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    StackTableViewCell *cell;
    
    if (cell == nil) {
    cell = (StackTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    
    _questionDictionary = [_JsonArray objectAtIndex:indexPath.row];
    NSString *questionRaw = [_questionDictionary valueForKey:@"title"];
    NSString *questionClean = [questionRaw stringByConvertingHTMLToPlainText];
    NSString *voteCountString = [NSString stringWithFormat:@"%@",[_questionDictionary valueForKey:@"score"]] ;
    NSArray *tagArray = [_questionDictionary valueForKey:@"tags"];
    NSMutableString *tagString = [NSMutableString string];
    
    for (NSString *string in tagArray) {
        NSString *stringFormatted = [NSString stringWithFormat:@" %@,", string];
        [tagString appendString:stringFormatted];
    }
    
    
    [cell.titleLabel setAdjustsFontSizeToFitWidth:YES];
    cell.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.titleLabel.numberOfLines = 0;
    cell.titleLabel.text = questionClean;
    cell.voteCount.text = voteCountString;
    cell.tags.text = tagString;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _questionDictionary = [_JsonArray objectAtIndex:indexPath.row];
    NSString *URLString = [_questionDictionary valueForKey:@"link"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)refreshControllerSetup {
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor blueColor];
    [_refreshControl addTarget:self
                        action:@selector(refresh)
              forControlEvents:UIControlEventValueChanged];
    [_stackTableView addSubview:_refreshControl];
}

@end
