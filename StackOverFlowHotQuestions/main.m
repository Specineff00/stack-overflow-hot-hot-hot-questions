//
//  main.m
//  StackOverFlowHotQuestions
//
//  Created by Yogesh N Ramsorrrun on 18/05/2016.
//  Copyright © 2016 Yogesh N Ramsorrrun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
